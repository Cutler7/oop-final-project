//
// Created by daniel on 29.11.18.
//

#include <iostream>
#include <cstdlib>
#include "Person.h"
#include "Date.h"
#include "Visit.h"
#include "System.h"
#include "Messages.h"
#include "PersonalData.h"
#include "AppNavigation.h"
#include "VisitsCalendar.h"

using namespace std;

int Dentist::dentistIdCounter = 0;
int Patient::patientIdCounter = 0;

int main (int argc, char *argv[]) {

    //APP INITIALIZATION
    FileOperator file = FileOperator(messagesFile);
    string result[100];
    file.readFile(result);
    Messages messagesStore = Messages(result, file.getNumberOfLines());
    AppNavigation appNavigator;

    //LOADING DATA
    TreatmentList treatmentStore = TreatmentList(messagesStore);
    treatmentStore.getDataFromFile();
    PersonList dentistList = PersonList(messagesStore, "Dentist List", DentistDataFile, DENTIST_FIELDS);
    dentistList.getDataFromFile();
    PersonList patientList = PersonList(messagesStore, "Patient List", PersonalDataFile, PATIENT_FIELDS);
    patientList.getDataFromFile();
    VisitsCalendar calendar = VisitsCalendar(messagesStore);

    //MAIN MENU
    int userChoice;
    do {
        system ("clear");
        userChoice = AppNavigation::displayMainMenu(messagesStore);
        appNavigator.handleMenuOpt(userChoice, messagesStore, treatmentStore, dentistList, patientList, calendar);
    } while (userChoice != 0);

    //SAVING DATA
    treatmentStore.saveDataIntoFile();
    dentistList.saveDataIntoFile();
    patientList.saveDataIntoFile();

    cout<<messagesStore.getMessage(MESSAGE_STR[GOODBYE_MSG])->content<<endl;
    return 1;
}
