//
// Created by daniel on 04.01.19.
//

#ifndef TEST_BUILD_PERSONALDATA_H
#define TEST_BUILD_PERSONALDATA_H

#include <string>
#include <set>
#include "Person.h"
#include "System.h"
#include "Messages.h"

#define TREATMENT_FIELDS 2

#define PATIENT_FIELDS 6

#define DENTIST_FIELDS 5

#define CURRENT_YEAR 2019

using namespace std;

class PersonList : public ListDataInterface, FileIoInterface {

    string listName;

    string fileName;

    int numberOfFields;

    set<Person*> personList;

    set<Person*>::iterator currentItem;

    Messages *messages;

    void addPerson(Person *person);

    void resetPointer();

    Person *getNextPerson();
public:

    void getPersonData(char personType);

    Person *getPersonById(int id);

    void listData();

    int getNumberOfElements() const;

    void saveDataIntoFile();

    void getDataFromFile();

    PersonList(Messages &messages, string listName, string fileName, int fieldsNumber);
};

class TreatmentList : public ListDataInterface, FileIoInterface {

    set<Treatment> treatments;

    set<Treatment>::iterator currentItem;

    Messages *messages;

    Treatment getNextTreatment();

    void resetPointer();

public:
    void addTreatment();

    Treatment *getTreatmentById(int id) const;

    void listData();

    int getNumberOfElements() const;

    void saveDataIntoFile();

    void getDataFromFile();

    TreatmentList(Messages &messages);
};

#endif //TEST_BUILD_PERSONALDATA_H
