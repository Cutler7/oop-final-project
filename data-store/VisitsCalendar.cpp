//
// Created by daniel on 04.01.19.
//

#include <string>
#include <set>
#include "VisitsCalendar.h"
#include "PersonalData.h"
#include "Messages.h"
#include "Visit.h"



using namespace std;

void VisitsCalendar::listData() {
    for (set<Visit>::iterator i = visits.begin(); i != visits.end(); i++) {
        i->printObject(*messages);
    }
}

void VisitsCalendar::addNewVisit(PersonList &dentistList, PersonList &patientList, TreatmentList &treatmentList) {
    Treatment *treatment;
    Patient *patient;
    Dentist *dentist;
    string date;
    string hour;
    int userInput;
    try {
        treatmentList.listData();
        cout << messages->getMessage(MESSAGE_STR[TREATMENT])->content << endl;
        cin >> userInput;
        treatment = treatmentList.getTreatmentById(userInput);

        dentistList.listData();
        cout << messages->getMessage(MESSAGE_STR[DENTIST])->content << endl;
        cin >> userInput;
        dentist = (Dentist*)dentistList.getPersonById(userInput);

        patientList.listData();
        cout << messages->getMessage(MESSAGE_STR[PATIENT])->content << endl;
        cin >> userInput;
        patient = (Patient*)patientList.getPersonById(userInput);
    } catch (string warning) {
        cout << warning << endl;
        return;
    }
    cout << messages->getMessage(MESSAGE_STR[GET_DATE])->content << endl;
    cin >> date;
    cout << messages->getMessage(MESSAGE_STR[GET_HOUR])->content << endl;
    cin >> date;
    Date dateObj = Date(atoi(date.substr(0, 2).c_str()), atoi(date.substr(3, 2).c_str()), CURRENT_YEAR,
                          atoi(hour.substr(0, 2).c_str()), atoi(hour.substr(3, 2).c_str()));
    visits.insert(Visit(treatment, dentist, patient, dateObj));
}

int VisitsCalendar::getNumberOfElements() const {
    return (int) visits.size();
}

void VisitsCalendar::resetPointer() {
    currentItem = visits.begin();
}

void VisitsCalendar::getVisitsByPatient(int patientId) {
    resetPointer();
    while (currentItem != visits.end()) {
        if (currentItem->getPatient()->getPersonId() == patientId) {
            currentItem->printObject(*messages);
        }
        currentItem++;
    }
}

void VisitsCalendar::getVisitsByDentist(int dentistId) {
    resetPointer();
    while (currentItem != visits.end()) {
        if (currentItem->getDentist()->getPersonId() == dentistId) {
            currentItem->printObject(*messages);
        }
        currentItem++;
    }
}

void VisitsCalendar::saveDataIntoFile() {
    FileOperator file = FileOperator(VisitsCalendarFile, 'w');
    resetPointer();
    int index = 0, numberOfVisits = getNumberOfElements();
    string *result = new string[numberOfVisits];
    for (set<Visit>::iterator i = visits.begin(); i != visits.end(); i++) {
        result[index] = i->toString();
    }
    file.writeFile(result, numberOfVisits);
}

void VisitsCalendar::getDataFromFile() {
    FileOperator file = FileOperator(VisitsCalendarFile);
    int numberOfVisits = file.getNumberOfLinesInFile();
    string *result = new string[numberOfVisits];
    file.readFile(result);
    string parsedFields[VISIT_FIELDS];
    int castFields[VISIT_FIELDS];
    for (int i = 0; i < numberOfVisits; i++) {
        file.parseData(parsedFields, result[i], ';', VISIT_FIELDS);
        for (int j = 0; j < VISIT_FIELDS; j++) {
            castFields[j] = atoi(parsedFields[j].c_str());
        }
        Date date = Date(castFields[0], castFields[1], castFields[2], castFields[3], castFields[4]);
        visits.insert(Visit(castFields[5], castFields[6], castFields[7], date));
    }
    resetPointer();
}

VisitsCalendar::VisitsCalendar(Messages &messages) {
    this->messages = &messages;
}
