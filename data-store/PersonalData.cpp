//
// Created by daniel on 04.01.19.
//

#include <string>
#include <iostream>
#include <algorithm>
#include "PersonalData.h"
#include "Date.h"

using namespace std;

// PERSON_LIST CLASS IMPLEMENTATION

void PersonList::addPerson(Person *person) {
    personList.insert(person);
}

void PersonList::getPersonData(char personType) {
    string name, surname, date;
    Person *newPerson = nullptr;
    float credit;
    cout << messages->getMessage(MESSAGE_STR[GET_NAME])->content << endl;
    cin >> name;
    cout << messages->getMessage(MESSAGE_STR[GET_SURNAME])->content << endl;
    cin >> surname;
    cout << messages->getMessage(MESSAGE_STR[BIRTH_DATE])->content << endl;
    cin >> date;
    if (personType == 'D') {
        newPerson = new Dentist(name, surname, atoi(date.substr(0, 2).c_str()),
                                atoi(date.substr(3, 2).c_str()), atoi(date.substr(6, 4).c_str()));
    } else {
        newPerson = new Patient(0, name, surname, atoi(date.substr(0, 2).c_str()),
                                atoi(date.substr(3, 2).c_str()), atoi(date.substr(6, 4).c_str()));
    }
    addPerson(newPerson);
}

Person *PersonList::getNextPerson() {
    if (currentItem == personList.end()) {
        throw (messages->getMessage(MESSAGE_STR[END_OF_LIST])->content);
    } else {
        Person *result = *currentItem;
        currentItem++;
        return result;
    }
}

Person *PersonList::getPersonById(int id) {
    set<Person *>::iterator it = find_if(personList.begin(), personList.end(),
                                         [&id](Person *p) { return p->getPersonId() == id; });
    if (it == personList.end()) {
        throw (messages->getMessage(MESSAGE_STR[NOT_FOUND])->content);
    } else {
        Person *result = *it;
        return result;
    }
}

void PersonList::resetPointer() {
    currentItem = personList.begin();
}

void PersonList::listData() {
    bool iterateThrough = true;
    cout << listName << "\n" << endl;
    while (iterateThrough) {
        try {
            getNextPerson()->printObject(*messages);
        } catch (string warning) {
            cout << warning << endl;
            resetPointer();
            iterateThrough = false;
        }
    }
}

int PersonList::getNumberOfElements() const {
    return (int) personList.size();
}

void PersonList::saveDataIntoFile() {
    FileOperator file = FileOperator(fileName, 'w');
    resetPointer();
    int index = 0, numberOfPerson = getNumberOfElements();
    string *result = new string[numberOfPerson];
    for (set<Person *>::iterator i = personList.begin(); i != personList.end(); i++) {
        result[index] = (*i)->toString();
    }
    file.writeFile(result, numberOfPerson);
}

void PersonList::getDataFromFile() {
    FileOperator file = FileOperator(fileName);
    int numberOfPerson = file.getNumberOfLinesInFile();
    string *result = new string[numberOfPerson];
    file.readFile(result);
    string parsedFields[numberOfFields];
    Person *createdObj = nullptr;
    for (int i = 0; i < numberOfPerson; i++) {
        file.parseData(parsedFields, result[i], ';', numberOfFields);
        if (numberOfFields == 5) {
            createdObj = new Dentist(parsedFields);
        } else {
            createdObj = new Patient(parsedFields);
        }
        personList.insert(createdObj);
    }
    resetPointer();
}

PersonList::PersonList(Messages &messages, string listName, string fileName, int fieldsNumber) {
    this->messages = &messages;
    this->listName = listName;
    this->fileName = fileName;
    this->numberOfFields = fieldsNumber;
    resetPointer();
}

// TREATMENT_LIST CLASS IMPLEMENTATION

void TreatmentList::addTreatment() {
    string newName;
    float newPrice;

    cout << messages->getMessage(MESSAGE_STR[TREATMENT])->content << endl;
    cout << messages->getMessage(MESSAGE_STR[GET_NAME])->content << " ";
    cin >> newName;
    cout << messages->getMessage(MESSAGE_STR[GET_PRICE])->content << " ";
    cin >> newPrice;

    treatments.insert(Treatment(newName, getNumberOfElements() + 1, newPrice));
}

Treatment *TreatmentList::getTreatmentById(int id) const {
    set<Treatment>::iterator it = find_if(treatments.begin(), treatments.end(),
                                          [&id](Treatment t) { return t.getTreatmentNo() == id; });
    if (it == treatments.end()) {
        throw (messages->getMessage(MESSAGE_STR[NOT_FOUND])->content);
    } else {
        Treatment *result = new Treatment();
        *result = *it;
        return result;
    }
}

void TreatmentList::resetPointer() {
    currentItem = treatments.begin();
}

Treatment TreatmentList::getNextTreatment() {
    if (currentItem != treatments.end()) {
        Treatment result = *currentItem;
        currentItem++;
        return result;
    } else {
        throw (messages->getMessage(MESSAGE_STR[END_OF_LIST])->content);
    }
}

void TreatmentList::listData() {
    bool iterateThrough = true;
    while (iterateThrough) {
        try {
            getNextTreatment().printObject(*messages);
        } catch (string warning) {
            cout << warning << endl;
            resetPointer();
            iterateThrough = false;
        }
    }
}

int TreatmentList::getNumberOfElements() const {
    return (int) treatments.size();
}

void TreatmentList::saveDataIntoFile() {
    FileOperator file = FileOperator(TreatmentDataFile, 'w');
    resetPointer();
    int index = 0, numberOfTreatments = getNumberOfElements();
    string *result = new string[numberOfTreatments];
    for (set<Treatment>::iterator i = treatments.begin(); i != treatments.end(); i++) {
        result[index] = i->toString();
    }
    file.writeFile(result, numberOfTreatments);
}

void TreatmentList::getDataFromFile() {
    FileOperator file = FileOperator(TreatmentDataFile);
    int numberOfTreatments = file.getNumberOfLinesInFile();
    string *result = new string[numberOfTreatments];
    file.readFile(result);
    string parsedFields[TREATMENT_FIELDS];
    for (int i = 0; i < numberOfTreatments; i++) {
        file.parseData(parsedFields, result[i], ';', TREATMENT_FIELDS);
        treatments.insert(Treatment(parsedFields[0], i + 1, (float) atof(parsedFields[1].c_str())));
    }
    resetPointer();
}

TreatmentList::TreatmentList(Messages &messages) {
    this->messages = &messages;
}
