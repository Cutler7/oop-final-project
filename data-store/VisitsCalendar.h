//
// Created by daniel on 04.01.19.
//

#ifndef TEST_BUILD_VISITSCALENDAR_H
#define TEST_BUILD_VISITSCALENDAR_H

#include <string>
#include <set>
#include "Person.h"
#include "System.h"
#include "Visit.h"
#include "PersonalData.h"

#define VISIT_FIELDS 8

using namespace std;

class VisitsCalendar: public ListDataInterface, FileIoInterface {

    set<Visit> visits;

    set<Visit>::iterator currentItem;

    Messages *messages;

public:
    void addNewVisit(PersonList &dentistList, PersonList &patientList, TreatmentList &treatmentList);

    void listData();

    int getNumberOfElements() const;

    void resetPointer();

    void getVisitsByPatient(int patientId);

    void getVisitsByDentist(int dentistId);

    void saveDataIntoFile();

    void getDataFromFile();

    VisitsCalendar(Messages &messages);
};
#endif //TEST_BUILD_VISITSCALENDAR_H
