//
// Created by daniel on 11.12.18.
//

#include "Date.h"
#include <string>

using namespace std;

string monthNumberToMonthName(int monthNo) {
    string result;
    switch (monthNo) {
        case MONTH_NAME(JANUARY) :
            result = "JANUARY";
            break;
        case MONTH_NAME(FEBRUARY) :
            result = "FEBRUARY";
            break;
        case MONTH_NAME(MARCH) :
            result = "MARCH";
            break;
        case MONTH_NAME(APRIL) :
            result = "APRIL";
            break;
        case MONTH_NAME(MAY) :
            result = "MAY";
            break;
        case MONTH_NAME(JUNE) :
            result = "JUNE";
            break;
        case MONTH_NAME(JULY) :
            result = "JULY";
            break;
        case MONTH_NAME(AUGUST) :
            result = "AUGUST";
            break;
        case MONTH_NAME(SEPTEMBER) :
            result = "SEPTEMBER";
            break;
        case MONTH_NAME(OCTOBER) :
            result = "OCTOBER";
            break;
        case MONTH_NAME(NOVEMBER) :
            result = "NOVEMBER";
            break;
        case MONTH_NAME(DECEMBER) :
            result = "DECEMBER";
            break;
    };
}

// DATE CLASS IMPLEMENTATION

string Date::getDate(char separator) const {
    return to_string(day) + separator + to_string(month) + separator + to_string(year);
}

string Date::getTime() const {
    return to_string(hour) + ":" + to_string(minutes);
}

int *Date::getTimeVector() const {
    int *result = new int[5];
    result[0] = this->year;
    result[1] = this->month;
    result[2] = this->day;
    result[3] = this->hour;
    result[4] = this->minutes;
    return result;
}

string Date::getFullDate(char separator) {
    return getDate(separator) + ", " + getTime();
}

string Date::getDateWithMonthName() {
    return to_string(day) + " " + monthNumberToMonthName(month) + " " + to_string(year);
}

string Date::toString() const {
    return getDate(';') + ";" + to_string(hour) + ";" + to_string(minutes) + ";";
}

void Date::printObject(Messages &messages) const {
    cout<<messages.getMessage(MESSAGE_STR[DATE])->content<<" "<<this->getDate('/')<<endl;
    if (hour != 0) {
        cout<<messages.getMessage(MESSAGE_STR[HOUR])->content<<" "<<this->getTime()<<endl;
    }
}

Date::Date() {

};

Date::Date(int day, int month, int year, int hour, int minutes) {
    this->day = day;
    this->month = month;
    this->monthName = monthNumberToMonthName(month);
    this->year = year;
    this->hour = hour;
    this->minutes = minutes;
}

Date::Date(int day, int month, int year) {
    this->day = day;
    this->month = month;
    this->monthName = monthNumberToMonthName(month);
    this->year = year;
    this->hour = 0;
    this->minutes = 0;
}

// TREATMENT CLASS IMPLEMENTATION

void Treatment::setTreatmentName(string name) {
    this->treatmentName = name;
}

string Treatment::getTreatmentName() const {
    return treatmentName;
}

void Treatment::setPrice(float price) {
    this->price = price;
}

float Treatment::getPrice() const {
    return price;
}

int Treatment::getTreatmentNo() const {
    return treatmentNo;
}

string Treatment::toString() const {
    return treatmentName + ";" + to_string(price) + ";";
}

void Treatment::printObject(Messages &messages) const {
    cout<<"--- "<<treatmentNo<<" ---"<<endl;
    cout<<messages.getMessage(MESSAGE_STR[GET_NAME])->content<<" "<<this->getTreatmentName()<<endl;
    cout<<messages.getMessage(MESSAGE_STR[GET_PRICE])->content<<" "<<this->getPrice()<<endl;
    cout<<"\n";
}

Treatment::Treatment(string name, int id, float price) {
    this->treatmentName = name;
    this->treatmentNo = id;
    this->price = price;
}

Treatment::Treatment(string name, int id) {
    this->treatmentName = name;
    this->treatmentNo = id;
}

Treatment::Treatment() {

}
