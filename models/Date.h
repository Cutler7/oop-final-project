//
// Created by daniel on 11.12.18.
//

#ifndef TEST_BUILD_DATE_H
#define TEST_BUILD_DATE_H

#include <string>
#include "System.h"

#define CURRENT_YEAR 2019

using namespace std;

enum MONTH_NAME
{
    JANUARY = 1,
    FEBRUARY = 2,
    MARCH = 3,
    APRIL = 4,
    MAY = 5,
    JUNE = 6,
    JULY = 7,
    AUGUST = 8,
    SEPTEMBER = 9,
    OCTOBER = 10,
    NOVEMBER = 11,
    DECEMBER = 12
};

string monthNumberToMonthName(int monthNo);

class Date : StringifyDataInterface  {

    int day;

    int month;

    string monthName;

    int year;

    int hour;

    int minutes;

public:
    string getDate(char separator) const;

    string getTime() const;

    int *getTimeVector() const;

    string getFullDate(char separator);

    string getDateWithMonthName();

    string toString() const;

    void printObject(Messages &messages) const;

    bool operator<(const Date &test) const {
        int *time = test.getTimeVector();
        if (year != time[0]) { return year < time[0]; }
        if (year != time[1]) { return year < time[1]; }
        if (year != time[2]) { return year < time[2]; }
        if (year != time[3]) { return year < time[3]; }
        if (year != time[4]) { return year < time[4]; }
    }

    Date();

    Date(int day, int month, int year, int hour, int minutes);

    Date(int day, int month, int year);
};

class Treatment : StringifyDataInterface {

    string treatmentName;

    int treatmentNo;

    float price;

public:
    void setTreatmentName(string name);

    string getTreatmentName() const;

    void setPrice(float price);

    float getPrice() const;

    int getTreatmentNo() const;

    string toString() const;

    void printObject(Messages &messages) const;

    bool operator<(const Treatment &t1) const { return treatmentNo < t1.getTreatmentNo(); }

    bool operator==(const Treatment &t1) const { return treatmentNo == t1.getTreatmentNo(); }

    Treatment(string name, int id, float price);

    Treatment(string name, int id);

    Treatment();
};

#endif //TEST_BUILD_DATE_H
