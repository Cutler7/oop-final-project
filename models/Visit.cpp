//
// Created by daniel on 19.01.19.
//

#include "Visit.h"
#include <string>
#include "System.h"
#include "Person.h"
#include "Date.h"

string Visit::toString() const {
    return date.toString() + ";" + to_string(treatment->getTreatmentNo()) + ";" +
           to_string(dentistId) + to_string(patientId);
}

void Visit::printObject(Messages &messages) const {
    cout<<"-------------------------------------"<<endl;
    treatment->printObject(messages);
    date.printObject(messages);
    cout<<messages.getMessage(MESSAGE_STR[PATIENT])->content<<endl;
    patient->printObject(messages);
    cout<<messages.getMessage(MESSAGE_STR[DENTIST])->content<<endl;
    dentist->printObject(messages);
}

Dentist *Visit::getDentist() const {
    return this->dentist;
}

Patient *Visit::getPatient() const {
    return this->patient;
}

Date Visit::getDate() const {
    return this->date;
}

Visit::Visit(Treatment *treatment, Dentist *dentist, Patient *patient, Date &date) {
    this->date = date;
    this->treatment = treatment;
    this->dentist = dentist;
    this->patient = patient;
}

Visit::Visit(int treatmentId, int dentistId, int patientId, Date &date) {
    this->date = date;
    this->treatmentId = treatmentId;
    this->dentistId = dentistId;
    this->patientId = patientId;
}

Visit::~Visit() {
    delete this->treatment;
    delete this->patient;
    delete this->dentist;
}
