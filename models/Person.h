//
// Created by daniel on 29.11.18.
//

#ifndef OOP_FINAL_PROJECT_PERSON_H
#define OOP_FINAL_PROJECT_PERSON_H

#include <string>
#include "Date.h"
#include "System.h"

using namespace std;

class Person : public StringifyDataInterface {

    string name;

    string surname;

    Date birthDate;

protected:
    int personId;

public:
    string getFullName() const;

    void setName(string name);

    void setSurname(string surname);

    Date getBirthDate() const;

    void setBirthDate(Date &birthDate);

    int getPersonId() const;

    string toString() const;

    virtual void printObject(Messages &messages) const;

    virtual int incrementId() { return 0; };

    bool operator<(const Person &test) const { return personId < test.getPersonId(); }

    bool operator==(const Person &test) const { return personId == test.getPersonId(); }

    Person();

    Person(string name, string surname, Date &birthDate);

    Person(string name, string surname, int day, int month, int year);

    Person(string *dataArr);
};

class Dentist : public Person {

    static int dentistIdCounter;

    int incrementId();

public:
    Dentist(string name, string surname, int day, int month, int year);

    Dentist(string *dataArr);

};

class Patient : public Person {

    static int patientIdCounter;

    float credit;

    int incrementId();

public:
    void setCredit(float credit);

    float getCredit() const;

    string toString();

    void printObject(Messages &messages) const;

    Patient(float credit, string name, string surname, Date &birthDate);

    Patient(float credit, string name, string surname, int day, int month, int year);

    Patient(string *dataArr);

};

#endif // OOP_FINAL_PROJECT_PERSON_H
