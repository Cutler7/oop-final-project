//
// Created by daniel on 29.11.18.
//

#include "Person.h"
#include "Date.h"
#include "Messages.h"
#include <string>

using namespace std;

// PERSON CLASS IMPLEMENTATION

string Person::getFullName() const {
    return name + " " + surname;
}

void Person::setName(string name) {
    this->name = name;
}

void Person::setSurname(string surname) {
    this->surname = surname;
}

Date Person::getBirthDate() const {
    return birthDate;
}

void Person::setBirthDate(Date &birthDate) {
    Person::birthDate = birthDate;
}

int Person::getPersonId() const {
    return personId;
}

string Person::toString() const {
    return  name + ";" + surname + ";" + birthDate.toString();
}

void Person::printObject(Messages &messages) const {
    cout<<"--- "<<personId<<" ---"<<endl;
    cout<<messages.getMessage(MESSAGE_STR[GET_NAME])->content<<" "<<this->getFullName()<<endl;
    birthDate.printObject(messages);
}

Person::Person() {
}

Person::Person(string name, string surname, Date &birthDate) {
    this->name = name;
    this->surname = surname;
    this->birthDate = birthDate;
}

Person::Person(string name, string surname, int day, int month, int year): birthDate(day, month, year) {
    this->name = name;
    this->surname = surname;
}

Person::Person(string *dataArr) {
    this->name = dataArr[0];
    this->surname = dataArr[1];
    int day = atoi(dataArr[2].c_str());
    int month = atoi(dataArr[3].c_str());
    int year = atoi(dataArr[4].c_str());
    this->birthDate = Date(day, month, year);
}

// PATIENT CLASS IMPLEMENTATION

int Dentist::incrementId() {
    Dentist::dentistIdCounter++;
    return Dentist::dentistIdCounter;
}

Dentist::Dentist(string name, string surname, int day, int month, int year)
        :Person(name, surname, day, month, year) {
    this->personId = incrementId();
}

Dentist::Dentist(string *dataArr)
        :Person(dataArr) {
    this->personId = incrementId();
}

// PATIENT CLASS IMPLEMENTATION

void Patient::setCredit(float credit) {
    this->credit = credit;
}

float Patient::getCredit() const {
    return credit;
}

string Patient::toString() {
    string result = Person::toString();
    result = result + to_string(credit) + ";";
    return result;
}

void Patient::printObject(Messages &messages) const {
    Person::printObject(messages);
    cout<<messages.getMessage(MESSAGE_STR[GET_CREDIT])->content<<" "<<this->getCredit()<<endl;
    cout<<"\n";
}

int Patient::incrementId() {
    Patient::patientIdCounter++;
    return Patient::patientIdCounter;
}

Patient::Patient(float credit, string name, string surname, Date &birthDate)
        :Person(name, surname, birthDate) {
    this->personId = incrementId();
    this->credit = credit;
}

Patient::Patient(float credit, string name, string surname, int day, int month, int year)
        :Person(name, surname, day, month, year) {
    this->personId = incrementId();
    this->credit = credit;
}

Patient::Patient(string *dataArr)
        :Person(dataArr) {
    this->personId = incrementId();
    this->credit = (float)atof(dataArr[5].c_str());
}
