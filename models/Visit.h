//
// Created by daniel on 19.01.19.
//

#ifndef TEST_BUILD_VISIT_H
#define TEST_BUILD_VISIT_H

#include <string>
#include "System.h"
#include "Person.h"
#include "Date.h"

class Visit : StringifyDataInterface {

    Date date;

    Treatment *treatment;

    Dentist *dentist;

    Patient *patient;

    int dentistId;

    int patientId;

    int treatmentId;

public:
    string toString() const;

    void printObject(Messages &messages) const;

    Dentist *getDentist() const;

    Patient *getPatient() const;

    Date getDate() const;

    bool operator<(const Visit &test) const { return date < test.getDate(); }

    Visit(Treatment *treatment, Dentist *dentist, Patient *patient, Date &date);

    Visit(int treatmentId, int dentistId, int patientId, Date &date);

    ~Visit();
};

#endif //TEST_BUILD_VISIT_H
