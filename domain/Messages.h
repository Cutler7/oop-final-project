//
// Created by daniel on 24.12.18.
//

#ifndef TEST_BUILD_MESSAGES_H
#define TEST_BUILD_MESSAGES_H

#include <string>

using namespace std;

const string messagesFile = "../app-data/system_messages.txt";
const string TreatmentDataFile = "../app-data/treatment_data.txt";
const string DentistDataFile = "../app-data/dentist_data.txt";
const string PersonalDataFile = "../app-data/personal_data.txt";
const string VisitsCalendarFile = "../app-data/visits_calendar.txt";

struct Message {
    string name;
    string content;
};

class Messages {

    Message *messages;

    int messagesCount;

public:
    Messages(string messages[], int messagesCount);

    Message *getMessage(string name);
};

enum MESSAGE_CODES {
    MENU_OPT_1,
    MENU_OPT_2,
    MENU_OPT_3,
    MENU_OPT_4,
    MENU_OPT_5,
    MENU_EXIT,
    SELECT_OPT,
    PUT_NUMBER_OF_OPT,
    STANDARD_ERROR,
    GOODBYE_MSG,
    DATE,
    HOUR,
    TIME,
    BIRTH_DATE,
    TREATMENT,
    GET_DATE,
    GET_HOUR,
    GET_NAME,
    GET_SURNAME,
    GET_TREATMENT_NAME,
    GET_PRICE,
    GET_CREDIT,
    DENTIST_LIST,
    PATIENT_LIST,
    TREATMENT_LIST,
    DENTIST_OR_PATIENT,
    END_OF_LIST,
    RETURN,
    SHOW,
    DENTIST,
    PATIENT,
    NOT_FOUND
};

extern const char *MESSAGE_STR[];

#endif //TEST_BUILD_MESSAGES_H
