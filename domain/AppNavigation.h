//
// Created by daniel on 06.01.19.
//

#ifndef TEST_BUILD_APPNAVIGATION_H
#define TEST_BUILD_APPNAVIGATION_H

#include "Messages.h"
#include "PersonalData.h"
#include "VisitsCalendar.h"

class AppNavigation {

public:
    static int displayMainMenu(Messages &messages);

    void handleMenuOpt(int opt, Messages &messages, TreatmentList &treatmentList, PersonList &dentistList,
            PersonList &patientList, VisitsCalendar &visitsCalendar);

    static int displayExploreDataMenu(Messages &messages);

    void handleExploreDataOpt(int opt, Messages &messages, TreatmentList &treatmentList, PersonList &dentistList,
            PersonList &patientList);
};

#endif //TEST_BUILD_APPNAVIGATION_H
