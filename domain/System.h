//
// Created by daniel on 11.12.18.
//

#ifndef TEST_BUILD_SYSTEM_H
#define TEST_BUILD_SYSTEM_H

#include <iostream>
#include <fstream>
#include <string>
#include "Messages.h"

using namespace std;

class FileOperator {

    fstream fileHandler;

    string line;

    int linesCount;

public:
    FileOperator(string fileName, char mode = 'r');

    void readFile(string *result);

    void writeFile(string content[], int size);

    string *parseData(string *result, string &line, char separator, int numberOfFields);

    int getNumberOfLines();

    int getNumberOfLinesInFile();
};

class StringifyDataInterface {

    virtual string toString() const = 0;

    virtual void printObject(Messages &messages) const = 0;
};

class ListDataInterface {

    virtual void listData() = 0;

    virtual int getNumberOfElements() const = 0;

    virtual void resetPointer() = 0;
};

class FileIoInterface {

    virtual void saveDataIntoFile() = 0;

    virtual void getDataFromFile() = 0;
};

#endif //TEST_BUILD_SYSTEM_H
