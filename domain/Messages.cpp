//
// Created by daniel on 24.12.18.
//

#include "Messages.h"
#include <string>

using namespace std;

Messages::Messages(string *messages, int messagesCount) {
    this->messagesCount = messagesCount;
    this->messages = new Message[messagesCount];
    int i = 0;
    for(i = 0; i < messagesCount; i+=2) {
        (this->messages + i)->name = *(messages + i);
        (this->messages + i)->content = *(messages + i + 1);
    }
}

Message* Messages::getMessage(string name) {
    int i = 0;
    for(i = 0; i < messagesCount; i++) {
        if ((this->messages + i)->name == name) {
            return (this->messages + i);
        }
    }
}

const char *MESSAGE_STR[] = {
        "MENU_OPT_1",
        "MENU_OPT_2",
        "MENU_OPT_3",
        "MENU_OPT_4",
        "MENU_OPT_5",
        "MENU_EXIT",
        "SELECT_OPT",
        "PUT_NUMBER_OF_OPT",
        "STANDARD_ERROR",
        "GOODBYE_MSG",
        "DATE",
        "HOUR",
        "TIME",
        "BIRTH_DATE",
        "TREATMENT",
        "GET_DATE",
        "GET_HOUR",
        "GET_NAME",
        "GET_SURNAME",
        "GET_TREATMENT_NAME",
        "GET_PRICE",
        "GET_CREDIT",
        "DENTIST_LIST",
        "PATIENT_LIST",
        "TREATMENT_LIST",
        "DENTIST_OR_PATIENT",
        "END_OF_LIST",
        "RETURN",
        "SHOW",
        "DENTIST",
        "PATIENT",
        "NOT_FOUND"
};
