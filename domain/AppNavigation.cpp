//
// Created by daniel on 06.01.19.
//

#include "AppNavigation.h"
#include "PersonalData.h"
#include "Messages.h"
#include "VisitsCalendar.h"

int AppNavigation::displayExploreDataMenu(Messages &messages) {
    int result;
    cout<<messages.getMessage(MESSAGE_STR[SELECT_OPT])->content<<endl;
    cout<<messages.getMessage(MESSAGE_STR[DENTIST_LIST])->content<<endl;
    cout<<messages.getMessage(MESSAGE_STR[PATIENT_LIST])->content<<endl;
    cout<<messages.getMessage(MESSAGE_STR[TREATMENT_LIST])->content<<endl;
    cout<<messages.getMessage(MESSAGE_STR[RETURN])->content<<endl;
    cout<<messages.getMessage(MESSAGE_STR[PUT_NUMBER_OF_OPT])->content;
    cin >> result;
    return result;
}

void AppNavigation::handleExploreDataOpt(int opt, Messages &messages, TreatmentList &treatmentList,
                                         PersonList &dentistList, PersonList &patientList) {
    switch (opt) {
        case 0:
            break;
        case 1:
            dentistList.listData();
            break;
        case 2:
            patientList.listData();
            break;
        case 3:
            treatmentList.listData();
            break;
        default:
            cout<<messages.getMessage(MESSAGE_STR[STANDARD_ERROR])->content<<endl;
            cin.ignore();
            cin.get();
            break;
    }
}

int AppNavigation::displayMainMenu(Messages &messages) {
    int result;
    cout<<messages.getMessage(MESSAGE_STR[SELECT_OPT])->content<<endl;
    cout<<messages.getMessage(MESSAGE_STR[MENU_OPT_1])->content<<endl;
    cout<<messages.getMessage(MESSAGE_STR[MENU_OPT_2])->content<<endl;
    cout<<messages.getMessage(MESSAGE_STR[MENU_OPT_3])->content<<endl;
    cout<<messages.getMessage(MESSAGE_STR[MENU_OPT_4])->content<<endl;
    cout<<messages.getMessage(MESSAGE_STR[MENU_OPT_5])->content<<endl;
    cout<<messages.getMessage(MESSAGE_STR[MENU_EXIT])->content<<endl;
    cout<<messages.getMessage(MESSAGE_STR[PUT_NUMBER_OF_OPT])->content;
    cin >> result;
    return result;
}

void AppNavigation::handleMenuOpt(int opt, Messages &messages, TreatmentList &treatmentList, PersonList &dentistList,
        PersonList &patientList, VisitsCalendar &visitsCalendar) {
    switch (opt) {
        case 0:
            break;
        case 1:
            visitsCalendar.addNewVisit(dentistList, patientList, treatmentList);
            break;
        case 2:
            char personChoice;
            cout<<messages.getMessage(MESSAGE_STR[DENTIST_OR_PATIENT])->content<<endl;
            cout<<messages.getMessage(MESSAGE_STR[PUT_NUMBER_OF_OPT])->content<<endl;
            cin>>personChoice;
            if (personChoice == 'D') {
                dentistList.getPersonData(personChoice);
            } else {
                patientList.getPersonData(personChoice);
            }
            break;
        case 3:
            treatmentList.addTreatment();
            break;
        case 4:
            int dentistId;
            dentistList.listData();
            cout<<messages.getMessage(MESSAGE_STR[PUT_NUMBER_OF_OPT])->content<<endl;
            cin>>dentistId;
            visitsCalendar.getVisitsByDentist(dentistId);
            break;
        case 5:
            int userChoice;
            userChoice = AppNavigation::displayExploreDataMenu(messages);
            handleExploreDataOpt(userChoice, messages, treatmentList, dentistList, patientList);
            break;
        default:
            cout<<messages.getMessage(MESSAGE_STR[STANDARD_ERROR])->content<<endl;
            cin.ignore();
            cin.get();
            break;
    }
}
