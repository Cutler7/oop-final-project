//
// Created by daniel on 11.12.18.
//

#include "System.h"
#include "Messages.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

FileOperator::FileOperator(string fileName, char mode) {
    if (mode == 'w') {
        fileHandler.open(fileName, ifstream::out | ifstream::trunc);
    } else {
        fileHandler.open(fileName);
    }
}

void FileOperator::readFile(string *result) {
    if (fileHandler.is_open()) {
        int i = 0;
        while ( getline (fileHandler,line) )
        {
            *(result + i) = line;
            i++;
        }
        fileHandler.close();
        this->linesCount = i;
    }
}

void FileOperator::writeFile(string content[], int size) {
    if (fileHandler.is_open()) {
        for(int i = 0; i < size; i++) {
            fileHandler<<content[i];
        }
        fileHandler.close();
    }
};

string *FileOperator::parseData(string *result, string &line, char separator, int numberOfFields) {
    int foundFields = 0, lastPosition = -1;
    size_t found;
    do {
        found = line.find(separator, (unsigned long)lastPosition + 1);
        result[foundFields] = line.substr((unsigned long)lastPosition + 1, (found - lastPosition) - 1);
        lastPosition = (int)found;
        foundFields++;
    } while (foundFields < numberOfFields);
    return result;
}

int FileOperator::getNumberOfLines() {
    return linesCount;
};

int FileOperator::getNumberOfLinesInFile() {
    int numberOfLines = 0;
    string line;
    while (getline (fileHandler,line)) ++numberOfLines;
    fileHandler.clear();
    fileHandler.seekg(0, ios::beg);
    return numberOfLines;
}
